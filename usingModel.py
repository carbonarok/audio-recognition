from numpy import loadtxt
from keras.models import load_model
import numpy as np
from scipy.io import wavfile
from scipy.signal import stft


def process_wav_file(x, threshold_freq=5500, eps=1e-10):
    # Read wav file to array
    wav = read_wav_file(x)
    # Sample rate
    L = 16000
    # If longer then randomly truncate
    if len(wav) > L:
        i = np.random.randint(0, len(wav) - L)
        wav = wav[i:(i+L)]  
    # If shorter then randomly add silence
    elif len(wav) < L:
        rem_len = L - len(wav)
        silence_part = np.random.randint(-100,100,16000).astype(np.float32) / np.iinfo(np.int16).max
        j = np.random.randint(0, rem_len)
        silence_part_left  = silence_part[0:j]
        silence_part_right = silence_part[j:rem_len]
        wav = np.concatenate([silence_part_left, wav, silence_part_right])
    # Create spectrogram using discrete FFT (change basis to frequencies)
    freqs, times, spec = stft(wav, L, nperseg = 400, noverlap = 240, nfft = 512, padded = False, boundary = None)
    # Cut high frequencies
    if threshold_freq is not None:
        spec = spec[freqs <= threshold_freq,:]
        freqs = freqs[freqs <= threshold_freq]
    # Log spectrogram
    amp = np.log(np.abs(spec)+eps)

    return np.expand_dims(amp, axis=2) 


def read_wav_file(x):
    # Read wavfile using scipy wavfile.read
    _, wav = wavfile.read(x) 
    # Normalize
    wav = wav.astype(np.float32) / np.iinfo(np.int16).max
        
    return wav


model = load_model('model.h5')

wav = process_wav_file('test.wav')
wav = np.reshape(wav, (1, 177, 98, 1))
prediction = model.predict(wav)

y_pred = np.argmax(prediction, axis=1)
print y_pred