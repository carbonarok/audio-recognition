from pydub import AudioSegment
from gtts import gTTS
import numpy as np
import wave
import random
import uuid
import os


def random_string(string_length=10):
    randomString = str(uuid.uuid4())
    randomString = randomString.upper()
    randomString = randomString.replace("-","")
    return randomString[0:string_length]


def generateTTS(speech, filename):
    tts = gTTS(speech, 'en', slow=False)
    tts.save('./trainingData/T' + filename + ".mp3")
    return


def convertMP3toWav(filename):
    sound = AudioSegment.from_file("./trainingData/T" + filename + ".mp3")
    sound.export('./trainingData/T' + filename + ".wav", format="wav")


def modifyWav(filename):
    wr = wave.open("./trainingData/T" + filename + ".wav", 'r')
    par = list(wr.getparams())
    par = tuple(par)

    ww = wave.open("./trainingData/M" + filename + ".wav", 'w')
    ww.setparams(par)

    fr = 400
    sz = wr.getframerate()//fr
    c = int(wr.getnframes()/sz)
    shift = 100//fr
    lossA = [40,50,60,70,80,90,100]
    loss = lossA[random.randint(0, len(lossA) - 1)]

    for num in range(c):
        da = np.fromstring(wr.readframes(sz), dtype=np.int16)
        left, right = da[0::2], da[1::2]
        lf, rf = np.fft.rfft(left), np.fft.rfft(right)
        lf, rf = np.roll(lf, shift), np.roll(rf, shift)
        lf[0:shift], rf[0:shift] = 0, 0
        nl, nr = np.fft.irfft(lf), np.fft.irfft(rf)
        ns = np.column_stack((nl, nr)).ravel().astype(np.int16)
        if random.randint(0, 32767) % loss != 0:
            ww.writeframes(ns.tostring())

    wr.close()
    ww.close()


def cleanup():
    os.system('rm ./trainingData/*.mp3')
    os.system('rm ./trainingData/T*.wav')
    return



if __name__ == "__main__":
    data = ["S", "T", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    iterations = 20
    for speech in data:
        for iteration in range(iterations):
            filename = "%s-%s" % (speech, random_string(6))
            generateTTS(speech, filename)
            convertMP3toWav(filename)
            modifyWav(filename)
    cleanup()
