from sklearn.metrics import accuracy_score
from keras.callbacks import EarlyStopping
from keras.models import Model
from keras.layers import Input, Dense, Dropout, Flatten
from dataset import DatasetGenerator
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization 
import numpy as np


def deep_cnn(features_shape, num_classes, act='relu'):
    x = Input(name='inputs', shape=features_shape, dtype='float32')
    o = x
    
    # Block 1
    o = Conv2D(32, (3, 3), activation=act, padding='same', strides=1, name='block1_conv', input_shape=features_shape)(o)
    o = MaxPooling2D((3, 3), strides=(2,2), padding='same', name='block1_pool')(o)
    o = BatchNormalization(name='block1_norm')(o)
    
    # Block 2
    o = Conv2D(32, (3, 3), activation=act, padding='same', strides=1, name='block2_conv')(o)
    o = MaxPooling2D((3, 3), strides=(2,2), padding='same', name='block2_pool')(o)
    o = BatchNormalization(name='block2_norm')(o)
 
    # Block 3
    o = Conv2D(32, (3, 3), activation=act, padding='same', strides=1, name='block3_conv')(o)
    o = MaxPooling2D((3, 3), strides=(2,2), padding='same', name='block3_pool')(o)
    o = BatchNormalization(name='block3_norm')(o)
 
    # Flatten
    o = Flatten(name='flatten')(o)
    
    # Dense layer
    o = Dense(64, activation=act, name='dense')(o)
    o = BatchNormalization(name='dense_norm')(o)
    o = Dropout(0.2, name='dropout')(o)
    
    # Predictions
    o = Dense(num_classes, activation='softmax', name='pred')(o)
 
    # Print network summary
    Model(inputs=x, outputs=o).summary()
    
    return Model(inputs=x, outputs=o)


DIR = './trainingData'
 
INPUT_SHAPE = (177,98,1)
BATCH = 32
EPOCHS = 15
 
LABELS = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "S", "T"]
NUM_CLASSES = len(LABELS)

dsGen = DatasetGenerator(label_set=LABELS)
df = dsGen.load_data(DIR)

dsGen.apply_train_test_split(test_size=0.3, random_state=2018)
dsGen.apply_train_val_split(val_size=0.2, random_state=2018)

model = deep_cnn(INPUT_SHAPE, NUM_CLASSES)
model.compile(optimizer='Adam', loss='categorical_crossentropy', metrics=['acc'])

callbacks = [EarlyStopping(monitor='val_acc', patience=4, verbose=1, mode='max')]
 
history = model.fit_generator(generator=dsGen.generator(BATCH, mode='train'),
                              steps_per_epoch=int(np.ceil(len(dsGen.df_train)/BATCH)),
                              epochs=EPOCHS,
                              verbose=1,
                              callbacks=callbacks,
                              validation_data=dsGen.generator(BATCH, mode='val'),
                              validation_steps=int(np.ceil(len(dsGen.df_val)/BATCH)))

y_pred_proba = model.predict_generator(dsGen.generator(BATCH, mode='test'), 
                                     int(np.ceil(len(dsGen.df_test)/BATCH)), 
                                     verbose=1)

y_pred = np.argmax(y_pred_proba, axis=1)
y_true = dsGen.df_test['label_id'].values
y_true = y_true[:len(y_pred) - len(y_true)]
print(y_pred)
print(y_true)

acc_score = accuracy_score(y_true, y_pred)
print(acc_score)

print("---------------")
print(" Pred  | True")
print("---------------")

for x in range(0, len(y_pred)):
    print("     %s | %s " % (LABELS[y_pred[x]], LABELS[y_true[x]]))

model.save("model.h5")
print("Model Saved!")
